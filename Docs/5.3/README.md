# Keppy Steinway Piano: The most realistic soundfont, for free
The project has been moved to GitHub, since Keppy Studios has been closed.

## Description
Are you tired of being forced to pay incredible amounts of money, just for a piano VST?
<br>
Do you want to just play standard MIDI files with good piano samples?
<br>
Well, this is the right project for you!
<br><br>
Welcome to the Keppy's Steinway Piano project page on GitHub, where you can download one of the best free piano soundfonts on the Internet!

## Main features
- Sampled from a Steinway D-274 Concert Grand!
- HQ samples, WAV 16bit, 128-point sinc, 1536kbps. (There are exceptions)
- Dynamic filtering, to emulate key pressures. (No velocity layers, ultrarealistic presets are an exception)
- Multiple presets for all the tastes!
- Can be edited as pleased with a generic notepad program! (Even the one built-in on Windows)

## Rules for the use of this project
1. You can mix this soundfont with other GM soundfonts, but not with other piano soundfonts. This applies to other soundfonts of mine too. (Ex. Mixing my Keppy's Steinway Piano with the Roland Dream Piano)
2. Give us proper credits if you're going to use it in musical projects or YouTube videos. Add these lines to your description: "This soundfont was made by KaleidonKep99 and Frozen Snow Productions, you can find it here: https://github.com/KaleidonKep99/Keppy-Steinway-Piano"
3. If you want to publish it on your website, you can embed the page through an IFRAME tag or by inserting a direct link to this page. Never use direct download links. NEVER.
4. You are allowed to add effects to the rendered audio (Ex. Rendering audio through SynthFont2 and then adding multiple effects through FL Studio), but you're not allowed to come to the point where the effects cover the original sound.
5. Don't implement these samples in your personal soundfonts.

## Why is it on GitHub?
As I said before, Keppy Studios has been closed, but I didn't want to kill this project.
<br>
So I gave it another chance, and moved it to GitHub.
<br><br>
Have fun.
<br>
~ Keppy