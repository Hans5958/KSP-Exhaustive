This soundfont is splitted in different versions.

Let me explain quick what are them:

Presets folder contains:
KSP - Concert Grand (Series): The standard one. There are different versions.
KSP - Bright Piano: Same thing as the previous one, but it's a bit more bright.

The damperless SFZ files are for MIDIs without MIDI events, to make them more realistic.

Trivia: You can actually use the non-Black MIDI ready soundfonts with real-time drivers, by using Keppy's MIDI Driver.

Hope you like my work! Enjoy!
~ Keppy and Frozy