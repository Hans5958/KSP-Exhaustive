# Keppy's Steinway Piano — Exhaustive

This is a repository to store various version of Keppy's Steinway Piano, also known as Steinway D-274.

For some reason, the original repositories of Keppy's Steinway Piano has been taken down. Thankfully, I have created my personal copy of it, with most versions changes are merged into one. The samples that is used are from the latest version, but several mirrors were found all way back to 5.1, which samples can be taken also. 

## Mirrors

If you wanted to see the original repositories, or if you want to switch the samples with another version, you can check out this repositories.

- https://gitlab.com/Hans5958-Mirrors/KSP/Keppy-Steinway-Piano  
  Hosting 5.1 to 6.27 (excluding 5.2). Mirrored from https://github.com/rastating/Keppy-Steinway-Piano/
- https://gitlab.com/Hans5958-Mirrors/KSP/Keppy-Steinway-Piano-5.2  
  Hosting 5.2. Mirrored from https://musical-artifacts.com/artifacts/543
- https://gitlab.com/Hans5958-Mirrors/KSP/Steinway-D-274  
  Hosting 7.0 to 7.4. Mirrored from https://gitee.com/airbeta/Steinway-D-274

## License

The version 5.1 to 6.27 is licensed under the terms of CC-BY 4.0.

The version 7.0 to 7.4 is licensed under the terms of CC-BY-SA 4.0.
